import Application from "./Application";
import { render, screen } from "@testing-library/react";

describe("Application", () => {
    // test for input element with type text and id name
  test("renders correctly", () => {
    // creates the virtual dom
    render(<Application />);
    const nameElement = screen.getByRole("textbox");
    expect(nameElement).toBeInTheDocument();
  });
});
